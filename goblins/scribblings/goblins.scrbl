#lang scribble/manual

@;; Copyright 2020 Christine Lemmer-Webber
@;;
@;; Licensed under the Apache License, Version 2.0 (the "License");
@;; you may not use this file except in compliance with the License.
@;; You may obtain a copy of the License at
@;;
@;;    http://www.apache.org/licenses/LICENSE-2.0
@;;
@;; Unless required by applicable law or agreed to in writing, software
@;; distributed under the License is distributed on an "AS IS" BASIS,
@;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@;; See the License for the specific language governing permissions and
@;; limitations under the License.

@title{Goblins: a transactional, distributed actor model environment}

@author{Christine Lemmer-Webber}

@emph{
  @bold{CAUTION:} Goblins is currently alpha.
  Semantics may change and you may be forced to update your code.
  Be mindful of that before using this for anything that goes into
  production!}

@table-of-contents[]

@include-section["intro.scrbl"]
@include-section["tutorial.scrbl"]
@include-section["api.scrbl"]
@include-section["actor-lib.scrbl"]
@include-section["captp.scrbl"]

