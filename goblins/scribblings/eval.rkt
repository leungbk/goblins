#lang racket

;;; Copyright 2020-2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require racket/sandbox
         scribble/manual
         scribble/example
         (prefix-in scribble-eval: scribble/eval))

(provide goblins-evaluator
         interact
         interact-errors
         delayed-interact
         delayed-interact-errors
         run-codeblock
         hidden-eval
         reset-eval!)

(define goblins-evaluator
  (make-base-eval #:lang 'racket))

(define (delayed-goblins-evaluator expr)
  (begin0 (goblins-evaluator expr)
    (sleep 0.02)))

(define-syntax-rule (interact e ...)
  (examples #:eval goblins-evaluator
            #:label #f
            e ...))

(define-syntax-rule (interact-errors e ...)
  (scribble-eval:interaction #:eval goblins-evaluator
                             e ...))

(define-syntax-rule (delayed-interact e ...)
  (examples #:eval delayed-goblins-evaluator
            #:label #f
            e ...))

(define-syntax-rule (delayed-interact-errors e ...)
  (scribble-eval:interaction #:eval delayed-goblins-evaluator
                             e ...))

(define-syntax-rule (run-codeblock e ...)
  (begin
    (goblins-evaluator
     (call-with-input-string (string-join (list "(begin " e ... " )") " ")
       read))
    (codeblock e ...)))

(define-syntax-rule (hidden-eval e ...)
  (examples #:eval goblins-evaluator
            #:hidden #t
            e ...))

(define (reset-eval!)
  (set! goblins-evaluator (make-base-eval #:lang 'racket)))
