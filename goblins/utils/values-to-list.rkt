#lang racket/base

;;; Copyright 2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide values->list)

(define-syntax-rule (values->list body ...)
  (call-with-values (lambda () body ...) list))

(module+ test
  (require rackunit)

  (test-equal?
   "values->list basic test"
   (values->list (let ([a 1]
                       [b 2])
                   (values a b)))
   '(1 2)))
