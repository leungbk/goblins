#lang racket/base

;;; Copyright 2020-2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require rackunit
         racket/async-channel
         racket/match
         "captp.rkt"
         "../vat.rkt"
         "../core.rkt"
         "structs-urls.rkt"

         "../actor-lib/bootstrap.rkt"
         "../actor-lib/joiners.rkt"
         "../actor-lib/methods.rkt"
         "netlayer/fake-intarwebs.rkt")

;;; Some utilities, used later

(define ((^greeter bcom my-name) your-name)
  (format "<~a> Hello ~a!" my-name your-name))

;;;; Here lie some old "poke it manually" tests.  We should maybe restore
;;;; these because they help check that the actual protocol works the way
;;;; we want, but maybe we should do that by building an actual ocapn
;;;; test harness.

;; ;; Testing against a machine representative with nothing real on the other side.
;; ;; We're calling our end the "repl" end even if inaccurate ;)
;; (define-values (repl->test1-ip repl->test1-op)
;;   (make-pipe))
;; (define-values (test1->repl-ip test1->repl-op)
;;   (make-pipe))
;; (define test1-vat (make-vat))
;; (define-vat-run test1-run test1-vat)

;; ;; We'll use this to see if the bootstrap object ever gets our message
;; (define test1-bootstrap-response-ch
;;   (make-channel))

;; (match-define (cons test1-registry test1-locator)
;;   (test1-vat 'run spawn-extended-registry-locator-pair))

;; ;; We add an extra method here just for testing for basic communication
;; (define test1-bootstrap-actor
;;   (test1-vat 'spawn
;;              (lambda (bcom)
;;                (methods
;;                 #:extends test1-locator
;;                 [(respond-to name)
;;                  (channel-put test1-bootstrap-response-ch
;;                               `(hello ,name))]
;;                 [(break-me)
;;                  (error 'ahhh-i-am-broken)]))))

;; (define test1-mycapn
;;   (test1-run (spawn-mycapn `#hasheq((imaginary . ,(spawn ^fake-intarwebs-netlayer
;;                                                                  #hasheq()
;;                                                                  'test1)))
;;                                    #:custom-bootstrap
;;                                    (make-extended-bootstrap test1-locator))))

;; (define _test1-bs-promise
;;   (test1-vat 'run
;;              (lambda ()
;;                ($ test1-mycapn 'new-connection
;;                   (ocapn-machine 'imaginary "repl" #f)
;;                   repl->test1-ip test1->repl-op))))

;; (define repl-handoff-privkey
;;   (generate-private-key eddsa
;;                         '((curve ed25519))))
;; (define repl-handoff-pubkey
;;   (pk-key->public-only-key repl-handoff-privkey))
;; (define repl-encoded-handoff-pubkey
;;   (pk-key->datum repl-handoff-pubkey 'rkt-public))

;; ;; First of all, we need to send our own end of the session
;; (let ()
;;   (syrup-write (mtp:op:start-session repl-encoded-handoff-pubkey
;;                                      (ocapn-machine 'imaginary "repl" #f)
;;                                      (pk-sign repl-handoff-privkey
;;                                               (syrup-encode
;;                                                (record* 'my-location
;;                                                         (ocapn-machine 'imaginary "repl" #f))
;;                                                #:marshallers marshallers)))
;;                repl->test1-op
;;                #:marshallers marshallers)
;;   (void))

;; ;; We should also hear its own side of the session
;; (test-true
;;  "test machine should send their part of the session"
;;  (mtp:op:start-session? (syrup-read test1->repl-ip
;;                                     #:unmarshallers unmarshallers)))

;; ;; It should send us (the REPL) a bootstrap message with answer pos 0
;; (test-equal?
;;  "test machine should send a bootstrap message"
;;  (syrup-read test1->repl-ip
;;              #:unmarshallers unmarshallers)
;;  (op:bootstrap 0 (desc:import-object 0)))

;; ;; Now we should bootstrap it such that it allocates an answer for us
;; (let ()
;;   (syrup-write (op:bootstrap 0 (desc:import-object 0))
;;                repl->test1-op
;;                #:marshallers marshallers)
;;   (void))

;; ;; Now we should be able to submit a test message
;; (let ()
;;   (syrup-write (op:deliver-only (desc:answer 0) #f '(respond-to REPL-friend) #hasheq())
;;                repl->test1-op
;;                #:marshallers marshallers)
;;   (void))

;; (test-equal?
;;  "bootstrap actor is able to get messages from us"
;;  (sync/timeout
;;   0.5
;;   test1-bootstrap-response-ch)
;;  '(hello REPL-friend))


;; First, set up the registry everyone will be using
(define-vat-run network-vat-run (make-vat))
(define network
  (network-vat-run
   (spawn ^fake-network)))

(define m1-vat
  (make-vat))
(define m2-vat
  (make-vat))
(define m3-vat
  (make-vat))

(define m1-incoming (make-async-channel))
(define m2-incoming (make-async-channel))
(define m3-incoming (make-async-channel))

(define-vat-run m1-run m1-vat)
(define-vat-run m2-run m2-vat)
(define-vat-run m3-run m3-vat)

(define m1-fake-netlayer
  (m1-run
   (spawn ^fake-netlayer "m1" network m1-incoming)))
(define m2-fake-netlayer
  (m2-run
   (spawn ^fake-netlayer "m2" network m2-incoming)))
(define m3-fake-netlayer
  (m3-run
   (spawn ^fake-netlayer "m3" network m3-incoming)))

(network-vat-run
 ($ network 'register "m1" m1-incoming)
 ($ network 'register "m2" m2-incoming)
 ($ network 'register "m3" m3-incoming))

;; Set up the machines we plan to use.
(define m1-location
  (string->ocapn-machine "ocapn:m.fake.m1"))
(define m2-location
  (string->ocapn-machine "ocapn:m.fake.m2"))
(define m3-location
  (string->ocapn-machine "ocapn:m.fake.m3"))

(define m1-fakenet
  (m1-run (spawn ^fake-netlayer "m1" network m1-incoming)))
(define m2-fakenet
  (m2-run (spawn ^fake-netlayer "m2" network m2-incoming)))
(define m3-fakenet
  (m3-run (spawn ^fake-netlayer "m3" network m3-incoming)))

(define m1-mycapn (m1-run (spawn-mycapn m1-fake-netlayer)))
(define m2-mycapn (m2-run (spawn-mycapn m2-fake-netlayer)))
(define m3-mycapn (m3-run (spawn-mycapn m3-fake-netlayer)))

;; Now we're going to set up the initial conditions.
;;
;;  M1        M2
;; [A C] --> [B]
;;   |
;;   |        M3
;;   '-----> [D]
;;
;; Later on, it'll be up to M2 and M3 to set up their own
;; connections.

;; TODO: Looks like we triggered the prodigal son mystery error.
;;
;; This is happening because we're connecting in both directions and
;; we've hit the "crossed hellos" problem...
;;
;; Our pseudo-solution, for now, is to delay between the two setups
;; so that we don't hit the race condition.  Obviously this is a klugy
;; workaround.  What we really need is to actually solve the crossed
;; hellos problem.  Removing the "sleep" statements below will re-trigger
;; it and in a sense open a good place for us to do a test.

;;
;; M1 <-> M2
(define m1->m2-vow
  (m1-run ($ m1-mycapn 'connect-to-machine m2-location)))
(sleep .2)
(define m2->m1-vow
  (m2-run ($ m2-mycapn 'connect-to-machine m1-location)))
;; ;; M1 <-> M3
(define m1->m3-vow
  (m1-run ($ m1-mycapn 'connect-to-machine m3-location)))
(sleep .2)
(define m3->m1-vow
  (m3-run ($ m3-mycapn 'connect-to-machine m1-location)))

;; Vat A -> Vat B tests
;; (TODO: Replace this with machines in separate places)
(define a-vat
  (make-vat))
(define b-vat
  (make-vat))
(define-vat-run a-run a-vat)
(define-vat-run b-run b-vat)

;; Now let's spawn bob in vat A
(define bob-greeter1
  (b-run (spawn ^greeter "Bob")))
(define bob-greeter1-sref
  (m2-run ($ m2-mycapn 'register bob-greeter1 'fake)))
(define bob-greeter1-sez-ch
  (make-async-channel))
(a-run
 (on (<- m1-mycapn 'enliven bob-greeter1-sref)
     (lambda (bob)
       (on (<- bob "Alyssa")
           (lambda (bob-sez)
             (async-channel-put bob-greeter1-sez-ch
                                `(fulfilled ,bob-sez)))))))
(test-equal?
 "Non-pipelined sends to bob work"
 (sync/timeout 0.2 bob-greeter1-sez-ch)
 '(fulfilled "<Bob> Hello Alyssa!"))

(define bob-greeter2
  (b-run (spawn ^greeter "Bob")))
(define bob-greeter2-sref
  (m2-run ($ m2-mycapn 'register bob-greeter2 'fake)))
(define bob-greeter2-sez-ch
  (make-async-channel))
(a-run
 (on (<- (<- m1-mycapn 'enliven bob-greeter2-sref)
         "Alyssa")
     (lambda (bob-sez)
       (async-channel-put bob-greeter2-sez-ch
                          `(fulfilled ,bob-sez)))))
(test-equal?
 "Pipelined sends to bob work"
 (sync/timeout 0.2 bob-greeter2-sez-ch)
 '(fulfilled "<Bob> Hello Alyssa!"))

;; For that matter, let's try invoking the fetch method directly
;; of the bootstrap actor
(define bob-greeter3-sez-ch
  (make-async-channel))
(a-run
 (on (<- m1->m2-vow 'fetch (ocapn-sturdyref-swiss-num bob-greeter1-sref))
     (lambda (bob)
       (on (<- bob "Alyssa")
           (lambda (bob-sez)
             (async-channel-put bob-greeter3-sez-ch
                                `(fulfilled ,bob-sez)))))))
(test-equal?
 "Non-pipelined sends to bob directly over fetched from bootstrap actor work"
 (sync/timeout 0.2 bob-greeter3-sez-ch)
 '(fulfilled "<Bob> Hello Alyssa!"))

(define ((^broken-greeter bcom my-name) your-name)
  (error 'oh-no-i-broke "My name: ~a" my-name)
  (format "<~a> Hello ~a!" my-name your-name))

(define broken-bob-greeter
  (b-run (spawn ^broken-greeter "Broken Bob")))
(define broken-bob-greeter-sref
  (m2-run ($ m2-mycapn 'register broken-bob-greeter 'fake)))
(define broken-bob-greeter-ch
  (make-async-channel))
(a-run
 (on (<- (<- m1-mycapn 'enliven broken-bob-greeter-sref)
         "Alyssa")
     (lambda (bob-sez)
       (async-channel-put broken-bob-greeter-ch
                          `(fulfilled ,bob-sez)))
     #:catch
     (lambda (err)
       (async-channel-put broken-bob-greeter-ch
                          `(broken ,err)))))

(test-true
 "Breakage correctly propagates across captp"
 (match (sync/timeout 0.2 broken-bob-greeter-ch)
   [(list 'broken problem)
    #t]
   [something-else
    #f]))

;; ;; For that matter, here's a simpler test
;; (define m1->m2-bootstrap-break-me-ch
;;   (make-async-channel))
;; (a-run
;;  (on (<- m1->m2-vow 'break-me)
;;      (lambda (val)
;;        (async-channel-put m1->m2-bootstrap-break-me-ch
;;                           `(fulfilled val)))
;;      #:catch
;;      (lambda (err)
;;        (async-channel-put m1->m2-bootstrap-break-me-ch
;;                           `(broken err)))))
;; (test-true
;;  "Breakage correctly propagates across captp, simple example"
;;  (match (sync/timeout 0.2 m1->m2-bootstrap-break-me-ch)
;;    [(list 'broken problem)
;;     #t]
;;    [something-else
;;     #f]))

;; Now for testing three vats across two machines.
;; This one, functionally, is actually running through a-vat's
;; machinetp system
(define c-vat
  (make-vat))

(define meeter-bob-response-ch
  (make-async-channel))

(define introducer-alice
  (a-vat 'spawn
         (lambda (bcom)
           (lambda (intro-bob intro-carol)
             (<-np intro-bob 'meet intro-carol)))))
(define meeter-bob
  (b-vat 'spawn
         (lambda (bcom)
           (methods
            [(meet new-friend)
             (on (<- new-friend 'hi-new-friend)
                 (lambda (heard-back)
                   (async-channel-put meeter-bob-response-ch
                                      `(heard-back ,heard-back))))]))))
(define (^chatty bcom my-name)
  (methods
   [(hi-new-friend)
    (list 'hello-back-from my-name)]))

(define chatty-carol
  (c-vat 'spawn ^chatty 'carol))

(define meeter-bob-sref
  (m2-vat 'call m2-mycapn 'register meeter-bob 'fake))

(a-run
 (on (<- m1-mycapn 'enliven meeter-bob-sref)
     (lambda (meeter-bob)
       (<-np introducer-alice
             meeter-bob
             chatty-carol))))

(test-equal?
 "A and C on one machine, B on another, with introductions"
 (sync/timeout 0.5 meeter-bob-response-ch)
 '(heard-back (hello-back-from carol)))

;; Simple promise pipelining test: the car factory
(define (^car-factory bcom company-name)
  (define ((^car bcom model color))
    (format "*Vroom vroom!*  You drive your ~a ~a ~a!"
            color company-name model))
  (define (make-car model color)
    (spawn ^car model color))
  make-car)

(define fork-motors
  (c-vat 'spawn ^car-factory "Fork"))
(define fork-motors-sref
  (m1-vat 'call m1-mycapn 'register fork-motors 'fake))

(define car-drive-result-ch
  (make-async-channel))
(b-run
 (define car-factory-vow
   (<- m2->m1-vow 'fetch (ocapn-sturdyref-swiss-num fork-motors-sref)))
 (define car-vow
   (<- car-factory-vow "Explorist" "blue"))
 (define drive-noise-vow
   (<- car-vow))
 (on drive-noise-vow
     (lambda (noise)
       (async-channel-put car-drive-result-ch
                          `(fulfilled ,noise)))
     #:catch
     (lambda (err)
       (async-channel-put car-drive-result-ch
                          `(broken ,err)))))
;; TODO: But we also want to check that the messages sent are what we
;;   thought what they should be from a protocol-message perspective...
(test-equal?
 "Promise pipelining works across vats"
 (sync/timeout 0.5 car-drive-result-ch)
 '(fulfilled "*Vroom vroom!*  You drive your blue Fork Explorist!"))


;; ;; WIP WIP WIP WIP WIP
;; (define ((^parrot bcom) . args)
;;   (pk 'bawwwwk args))
;; (define parrot (b-vat 'spawn ^parrot))

;; ;; TODO: This apparently will need to register itself with the base
;; ;;   ^machine...
;; (define machine-representative->machine-thread-ch
;;   (make-machinetp-thread m2->m1-ip m1->m2-op
;;                          b-vat
;;                          bob))

;; (syrup-write (op:deliver-only 0 #f '("George") #hasheq())
;;              m2->m1-op
;;              #:marshallers marshallers)


(define ((^args-counter bcom) . args)
  (length args))
(define a-args-counter
  (a-vat 'spawn ^args-counter))
(define shares-a-args-counter
  (a-vat 'spawn
         (lambda (bcom)
           (lambda ()
             a-args-counter))))
(define shares-a-args-counter-sref
  (m1-vat 'call m1-mycapn 'register shares-a-args-counter 'fake))
;; vow on b specifically
(define shares-a-args-counter-vow
  (b-run
   (<- m2->m1-vow 'fetch (ocapn-sturdyref-swiss-num shares-a-args-counter-sref))))


;; Now for the three-machine handoff.
;; ==================================
(define d-vat
  (make-vat))
(define-vat-run d-run d-vat)

(define daniel
  (d-run (spawn ^chatty 'daniel)))
(define daniel-sref
  (m3-run ($ m3-mycapn 'register daniel 'fake)))

(a-run
 (define bob-vow (<- m1->m2-vow 'fetch (ocapn-sturdyref-swiss-num meeter-bob-sref)))
 (define daniel-vow (<- m1->m3-vow 'fetch (ocapn-sturdyref-swiss-num daniel-sref)))
 (on (all-of bob-vow daniel-vow )
     (match-lambda
       [(list bob daniel)
        (<- bob 'meet daniel)])))

;; Handoff success
(test-equal?
 "A on one machine, B on another, introduced to D on another, via handoffs"
 (sync/timeout 0.5 meeter-bob-response-ch)
 '(heard-back (hello-back-from daniel)))

;; Test for proper shortening ("unwrapping" back into the original
;; context).
;; The child should "come home", ie be eq? to itself when resolved
;; (as returned).
;; TODO: Curiously, it turns out this works, but not in the way I
;;   thought it did.  The handoff where M3 gifts prodigal-child exported
;;   from M1 to M2 means that M2 already has an export from M1.
;;   We've left a stub for maybe where this might happen in the
;;   "Shortening when handoffs come back home mystery error".
;;   That's where we might fill in further code... but is it ever
;;   needed?  The handoff from M3 gifting access to M2 already means
;;   that we've reduced things to a two-machine scenario... no need
;;   for another handoff.
;;   Nonetheless this test is really useful and should be preserved!
;; TODO UPDATE: And we did hit it!  Wee comment sabove about the
;;   race condition seen in the m1->m2-vow etc section

(define ((^prodigal-child _bcom))
  "Am I home yet?")
(define (^intermediate-land _bcom)
  (methods
   [(dear-child-come-home farthest-land)
    (<- farthest-land 'child-come-home)]))
(define (^farthest-land _bcom lost-child)
  (methods
   ;; return the lost child
   [(child-come-home) lost-child]))
(define ((^parent _bcom child) intermediate-land farthest-land result-ch)
  (on (<- intermediate-land 'dear-child-come-home farthest-land)
      (lambda (returned-child)
        (async-channel-put result-ch
                           `(the-child-returned
                             is-it-the-same? ,(eq? returned-child child))))
      #:catch
      (lambda (err)
        (async-channel-put result-ch
                           `(broken ,err)))))
(define a-prodigal-child
  (a-run (spawn ^prodigal-child)))
(define a-prodigal-sref
  (m1-run ($ m1-mycapn 'register a-prodigal-child 'fake)))
(define b-intermediate-land
  (b-run (spawn ^intermediate-land)))
(define b-intermediate-sref
  (m2-run ($ m2-mycapn 'register b-intermediate-land 'fake)))
(define d-farthest-land
  (d-run (spawn ^farthest-land
                (<- m3->m1-vow 'fetch
                    (ocapn-sturdyref-swiss-num a-prodigal-sref)))))
(define d-farthest-sref
  (m3-run ($ m3-mycapn 'register d-farthest-land 'fake)))

(define a-parent
  (a-run (spawn ^parent a-prodigal-child)))

(define prodigal-return-ch
  (make-async-channel))

;; call the child home
(a-run ($ a-parent
          (<- m1->m2-vow 'fetch (ocapn-sturdyref-swiss-num b-intermediate-sref))
          (<- m1->m3-vow 'fetch (ocapn-sturdyref-swiss-num d-farthest-sref))
          prodigal-return-ch))
;; did they arrive?
(test-equal?
 "Unwrapping/shortening on returning home via handoffs"
 (sync/timeout 0.5 prodigal-return-ch)
 '(the-child-returned is-it-the-same? #t))

(collect-garbage)

;; check that incremental gc is working
#;(b-vat 'run
         (lambda ()
           (on (<- shares-a-args-counter-vow)
               (lambda (a-args-counter)
                 (<- a-args-counter
                     bob-greeter2 bob-greeter2 bob-greeter2
                     bob-greeter2 bob-greeter2 bob-greeter2
                     bob-greeter2 bob-greeter2 bob-greeter2)))))
#;(collect-garbage)
;; ... damn we actually need to test the messages sent at some point

