#lang racket/base

;;; Copyright 2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require "../../core.rkt"
         "../../vat.rkt"
         "../../actor-lib/common.rkt"
         "../../actor-lib/methods.rkt"
         "../../actor-lib/sync-pr.rkt"
         "../structs-urls.rkt"
         "utils/read-write-procs.rkt"
         racket/async-channel
         racket/match)

(provide ^fake-network
         ^fake-netlayer)

(define (^fake-network _bcom)
  (define routes (spawn ^hash))
  (methods
   [(register name new-conn-ch)
    ($ routes 'set name new-conn-ch)]
   [(connect-to name)
    (define m2-new-conn-ch
      ($ routes 'ref name))
    (define-values (m1->m2-ip m1->m2-op)
      (make-pipe))
    (define-values (m2->m1-ip m2->m1-op)
      (make-pipe))
    ;; Send the connecting-to listener the channels it'll listen on
    (async-channel-put m2-new-conn-ch (vector m2->m1-ip m1->m2-op))
    ;; And send the connecting-from caller back its appropriate channels
    (vector m1->m2-ip m2->m1-op)]))

(define (^fake-netlayer _bcom our-name network new-conn-ch)
  (define our-location (ocapn-machine 'fake our-name #f))
  ;; This allows for incoming messages over a channel, preserving some illusion
  ;; of foreign-communication
  (define (start-listening conn-establisher)
    (let listen-loop ()
      (on (sync/pr new-conn-ch)
          (match-lambda
            [(list (vector ip op))
             (define-values (read-message write-message)
               (read-write-procs ip op))
             ;; TODO
             (<- conn-establisher read-message write-message #t)])
          #:finally listen-loop)))

  (define (^netlayer bcom)
    (define base-beh
      (methods
       [(netlayer-name) 'fake]
       [(our-location) our-location]))

    ;; State of the netlayer before it gets called with 'setup
    (define pre-setup-beh
      (methods
       #:extends base-beh
       ;; The machine is now wiring us up with the appropriate behavior for
       ;; when a new connection comes in
       [(setup conn-establisher)
        (start-listening conn-establisher)
        ;; Now that we're set up, transition to the main behavior
        (bcom (ready-beh conn-establisher))]))
    
    (define (ready-beh conn-establisher)
      (methods
       #:extends base-beh
       [(self-location? loc)
        (same-machine-location? our-location loc)]
       [(connect-to remote-machine)
        (match remote-machine
          [(ocapn-machine 'fake name #f)
           (on (<- network 'connect-to name)
               (match-lambda
                 [(vector ip op)
                  (define-values (read-message write-message)
                    (read-write-procs ip op))
                  (<- conn-establisher read-message write-message #f)])
               #:promise? #t)])]))
    ;; TODO: return pre-setup-beh
    pre-setup-beh)
  (spawn ^netlayer))


(module+ test
  (require rackunit
           "../captp.rkt"
           "../../actor-lib/bootstrap.rkt")
  ;; First, set up the registry everyone will be using
  (define-vat-run network-vat-run (make-vat))
  (define network
    (network-vat-run
     (spawn ^fake-network)))

  (define-vat-run a-run (make-vat))
  (define-vat-run b-run (make-vat))

  (define a-incoming (make-async-channel))
  (define b-incoming (make-async-channel))

  (define a-fake-netlayer
    (a-run
     (spawn ^fake-netlayer "a" network a-incoming)))
  (define b-fake-netlayer
    (b-run
     (spawn ^fake-netlayer "b" network b-incoming)))

  (network-vat-run
   ($ network 'register "a" a-incoming)
   ($ network 'register "b" b-incoming))

  (define a-location (string->ocapn-machine "ocapn:m.fake.a"))
  (define b-location (string->ocapn-machine "ocapn:m.fake.b"))

  (define a-mycapn
    (a-run
     (spawn-mycapn a-fake-netlayer)))
  (define b-mycapn
    (b-run
     (spawn-mycapn b-fake-netlayer)))

  (define a->b-vow
    (a-run ($ a-mycapn 'connect-to-machine b-location)))
  (define b->a-vow
    (b-run ($ b-mycapn 'connect-to-machine a-location)))

  ;; TODO: Next we need to spawn a couple of useful actors, make
  ;; sturdyrefs to them, and make sure we can message them from each
  ;; other.
  (define (^greeter _bcom my-name)
    (lambda (your-name)
      (format "Hello ~a, my name is ~a!" your-name my-name)))

  (define alice
    (a-run (spawn ^greeter "Alice")))
  (define bob
    (b-run (spawn ^greeter "Bob")))

  (define alice-locator-sref
    (a-run ($ a-mycapn 'register alice 'fake)))
  #;(define alice-sturdyref
    (ocapn-sturdyref a-location alice-locator-nonce))

  (define bob-locator-sref
    (b-run ($ b-mycapn 'register bob 'fake)))
  #;(define bob-sturdyref
    (ocapn-sturdyref b-location bob-locator-nonce))

  (let ([response-ch (make-async-channel)])
    (a-run
     (define bob-vow (<- a-mycapn 'enliven bob-locator-sref))
     (on (<- bob-vow "Arthur")
         (λ (val)
           (async-channel-put response-ch `(fulfilled ,val)))
         #:catch
         (λ (err)
           (async-channel-put response-ch `(broken ,err)))))
    (test-equal?
     "a->b communication works"
     (sync/timeout 0.5 response-ch)
     '(fulfilled "Hello Arthur, my name is Bob!")))

  (let ([response-ch (make-async-channel)])
    (b-run
     (define alice-vow (<- b-mycapn 'enliven alice-locator-sref))
     (on (<- alice-vow "Ben")
         (λ (val)
           (async-channel-put response-ch `(fulfilled ,val)))
         #:catch
         (λ (err)
           (async-channel-put response-ch `(broken ,err)))))
    (test-equal?
     "b->a communication works"
     (sync/timeout 0.5 response-ch)
     '(fulfilled "Hello Ben, my name is Alice!"))))

