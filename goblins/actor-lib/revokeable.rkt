#lang racket/base

;;; Copyright 2019-2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide spawn-revokeable)

(require "../core.rkt"
         "cell.rkt"
         "select-swear.rkt")

(define (spawn-revokeable target)
  (define $/<- (select-$/<- target))
  (define revoked?
    (spawn-cell #f))
  (define (^forwarder bcom)
    (make-keyword-procedure
     (lambda (kws kw-args . args)
       (when ($ revoked?)
         (error "Access revoked!"))
       (keyword-apply $/<- kws kw-args target args))))
  (define ((^revoker bcom))
    ($ revoked? #t))
  (values (spawn ^forwarder) (spawn ^revoker)))

(module+ test
  (require rackunit
           racket/match
           racket/contract
           "../utils/values-to-list.rkt")
  (define am (make-actormap))
  (define royal-admission
    (actormap-spawn!
     am (lambda (bcom)
          (lambda _
            "The Queen will see you now."))))
  (match-define (list royal-forwarder royal-revoker)
    (actormap-run! am
                   (lambda ()
                     (values->list (spawn-revokeable royal-admission)))))

  (check-equal?
   (actormap-peek am royal-forwarder)
   "The Queen will see you now.")

  (actormap-poke! am royal-revoker)

  (check-exn
   any/c
   (lambda () (royal-forwarder))))
