#lang racket/base

;;; Copyright 2020-2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide define-vat-run
         define-actormap-run)

(require "../core.rkt"
         syntax/parse/define
         (for-syntax racket/base
                     syntax/parse))

(define-simple-macro (define-vat-run id vat)
  (begin
    (define this-vat vat)
    (define-syntax (id vat-run-stx)
      (define body-stx (cdr (syntax-e vat-run-stx)))
      #`(this-vat 'run
                  (lambda ()
                    #,@body-stx)))))

(define-syntax-rule (define-actormap-run id actormap)
  (begin
    (define this-actormap actormap)
    (define-syntax (id am-run-stx)
      (define body-stx (cdr (syntax-e am-run-stx)))
      #`(actormap-run! this-actormap
                       (lambda ()
                         #,@body-stx)))))

(module+ test
  (require rackunit
           "../vat.rkt")
  (define a-vat (make-vat))
  (define-vat-run a-vat-run
    a-vat)
  (check-equal?
   (a-vat-run
    (define a-frond
      (spawn (lambda (bcom) (lambda () 'hello-frond))))
    ($ a-frond))
   'hello-frond)

  (define am (make-actormap))
  (define-actormap-run am-run am)
  
  (check-equal?
   (am-run
    (define a-frog
      (spawn (lambda (bcom) (lambda () 'hello-frog))))
    ($ a-frog))
   'hello-frog))
